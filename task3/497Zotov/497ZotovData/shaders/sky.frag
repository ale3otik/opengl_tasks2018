#version 330
uniform sampler2D skyTex;

in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
out vec4 fragColor; //выходной цвет фрагмента

void main()
{
    vec3 color = texture(skyTex, texCoord).rgb;
    fragColor = vec4(color, 1.0);
}

