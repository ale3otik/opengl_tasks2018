set(SRC_FILES
        Main.cpp
        SurfaceApplication.hpp
        SurfaceApplication.cpp
        common/Application.hpp
        common/Application.cpp
        common/Camera.hpp
        common/Camera.cpp
        common/Mesh.hpp
        common/Mesh.cpp
        common/ShaderProgram.hpp
        common/ShaderProgram.cpp
        common/PerlinNoise.hpp
        common/PerlinNoise.cpp
        )
include_directories(common)
MAKE_TASK(493Latypov 1 "${SRC_FILES}")