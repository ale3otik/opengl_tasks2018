#pragma once

#include <iostream>
#include <random>
#include <vector>

#include <Application.hpp>
#include <Common.h>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include "LSystem.h"

struct MaterialInfo {
    glm::vec3 Ka;
    glm::vec3 Kd;
    glm::vec3 Ks;
};

class TreeApplication : public Application {
    ShaderProgramPtr shader;
    ShaderProgramPtr leafShader; // without normal mapping

    std::mt19937 gen;
    std::uniform_real_distribution<float> angleDistribution;
    std::uniform_real_distribution<float> spaceDistribution;

    //Координаты источника света
    float _phi = 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;

    MaterialInfo leafMaterial;
    MaterialInfo barkMaterial;

    std::vector<ConeParams> conesParams;
    std::vector<MeshPtr> cones;
    std::vector<glm::vec3> leaves;
    std::vector<glm::vec3> leavesNormal;
    std::vector<glm::vec2> leavesTextureCoords;
    MeshPtr leavesMesh;

    TexturePtr barkTexture;
    TexturePtr normalMapTexture;
    TexturePtr leafTexture;

    GLuint barkSampler;
    GLuint normalMapSampler;
    GLuint leafSampler;

    float leafThicknessBound;
    float leafLength;
    float leafWidth;
    size_t leafNum;
private:
    void addLeaf(const ConeParams& params, bool drawOnTop);
public:
    void setLeafParams(
        float thicknessBound,
        float width,
        float length,
        size_t leafNumber
    );
    void setupLSystem(
        LSystem& system,
        uint16_t numIterations
    );
    void makeScene() override;
    void draw() override;
    void updateGUI() override;
};
