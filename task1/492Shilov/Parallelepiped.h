#pragma once

#include <glm/glm.hpp>
#include <Mesh.hpp>


struct ParallelepipedParams {
    glm::vec3 firstPoint;
    glm::vec3 secondPoint;
    glm::vec3 axis;
    glm::vec3 center;
    float width;
    float angle;
    float thickness;
};


MeshPtr getParallelepiped(const ParallelepipedParams& params);
