set(SRC_FILES
        Main.cpp
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp

        common/Application.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/DebugOutput.h
        )

set(SHADER_FILES
        491IvakhnenkoData/shader.frag
        491IvakhnenkoData/shader.vert
        )

source_group("Shaders" FILES
        ${SHADER_FILES}
        )

include_directories(common)

MAKE_TASK(491Ivakhnenko 1 "${SRC_FILES}")