include_directories("common/")

set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/Texture.cpp
    common/ShaderProgram.cpp
    common/DebugOutput.cpp
    Maze.h
    Main.cpp
)


MAKE_TASK(492Zinov 2 "${SRC_FILES}")

