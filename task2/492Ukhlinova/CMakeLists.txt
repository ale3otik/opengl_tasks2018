set(SRC_FILES
    Common.h
    Camera.h
    Texture.h
    Texture.cpp
    LightInfo.h
    Mesh.h
    Mesh.cpp
    ShaderProgram.cpp
    ShaderProgram.h
    Camera.cpp
    DebugOutput.h
    DebugOutput.cpp
    Application.h
    Application.cpp
    Main.h
    Main.cpp
)

MAKE_TASK(492Ukhlinova 2 "${SRC_FILES}")

