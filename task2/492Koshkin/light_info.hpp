#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

struct LightInfo {
    glm::vec3 direction; // Направление в мировой системе координат
    glm::vec3 ambient; // Цвет окружающего света
    glm::vec3 diffuse; // Диффузный цвет
    glm::vec3 specular; // Бликовый цвет
};
