/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;

struct PointLightInfo
{
    vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
};

struct SpotLightInfo
{
    vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
	vec3 dir; //направление источника света в системе координат виртуальной камеры
    float cosAngle; //косинус угла расхождения лучей конуса
};

uniform PointLightInfo light;
uniform SpotLightInfo cameraLight;
uniform PointLightInfo anotherLight;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

vec3 getLighting(vec3 initialColor, vec3 pos, vec3 La, vec3 Ld, vec3 Ls) { //получить цвет точки с заданным исходным цветом при освещении с заданными параметрами
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
    
	vec3 lightDirCamSpace = normalize(pos - posCamSpace.xyz); //направление на источник света 
	
	vec3[2] normals;
    normals[0] = normalize(normalCamSpace);
    normals[1] = -normals[0];

	vec3 color = 0;

	for (int i = 0; i < 2; ++i) {
		float NdotL = max(dot(normals[i], lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

		color += initialColor * (La + Ld * NdotL);

		if (NdotL > 0.0)
		{           
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

			float blinnTerm = max(dot(normals[i], halfVector), 0.0); //интенсивность бликового освещения по Блинну              
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
			color += Ls * Ks * blinnTerm;
		}
	}

	return color;
}

void main()
{

	vec3 textureColor = texture(diffuseTex, texCoord).rgb;
	vec3 color = getLighting(textureColor, light.pos, light.La, light.Ld, light.Ls); //освещаем точечным источником

    float cameraDirectionDotFragmentDirection = -dot(normalize(posCamSpace.xyz), cameraLight.dir); //скалярное произведение (косинус) между направлением камеры и направлением от камеры к фрагменту
	
	if(cameraDirectionDotFragmentDirection > cameraLight.cosAngle) { //фрагмент попадает в конус света от конического источника, закреплённого на камере
		color += getLighting(textureColor, cameraLight.pos, cameraLight.La, cameraLight.Ld, cameraLight.Ls); //освещаем коническим источником, закреплённым на камере
	}
	
    color += getLighting(textureColor, anotherLight.pos, anotherLight.La, anotherLight.Ld, anotherLight.Ls); //освещаем вторым точечным источником

    fragColor = vec4(color, 1.0);
}
