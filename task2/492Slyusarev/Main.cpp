#include "Application.h"
#include "Main.h"
#include "Mesh.h"
#include "ShaderProgram.h"
#include "LightInfo.h"
#include "Texture.h"

#include <vector>
#include <iostream>

class SampleApplication : public Application
{
public:
	MeshPtr _moebiusStrip;
	MeshPtr _marker; //������ ��� ��������� �����
	MeshPtr _anotherMarker; //������ ��� ������� ��������� �����

					 //������������� ��������� ���������
	ShaderProgramPtr _shader;
	ShaderProgramPtr _markerShader;
	ShaderProgramPtr _anotherMarkerShader;

	//���������� ��� ���������� ���������� ������ ��������� �����
	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	float _lr1 = 3.0;
	float _phi1 = 0.0;
	float _theta1 = 0.0;

	LightInfo _pointLight;
	SpotLightInfo _cameraLight;
	LightInfo _anotherLight;
	TexturePtr _texture;

	GLuint _sampler;

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//�������� � �������� �����		

		_moebiusStrip = makeMoebiusStrip(0.5f);
		_moebiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		_marker = makeSphere(0.1f);
		_anotherMarker = makeSphere(0.1f);

		//=========================================================
		//������������� ��������

		_shader = std::make_shared<ShaderProgram>("492SlyusarevData/texture.vert", "492SlyusarevData/texture.frag");
		_markerShader = std::make_shared<ShaderProgram>("492SlyusarevData/marker.vert", "492SlyusarevData/marker.frag");
		_anotherMarkerShader = std::make_shared<ShaderProgram>("492SlyusarevData/marker.vert", "492SlyusarevData/marker.frag");

		//=========================================================
		//������������� �������� ���������� ��������
		_pointLight.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_pointLight.ambient = glm::vec3(0.2, 0.2, 0.2);
		_pointLight.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_pointLight.specular = glm::vec3(1.0, 1.0, 1.0);

		_cameraLight.ambient = glm::vec3(0.2, 0.2, 0.2);
		_cameraLight.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_cameraLight.specular = glm::vec3(1.0, 1.0, 1.0);
		_cameraLight.angle = glm::pi<float>() * 0.125f;

		_anotherLight.position = glm::vec3(glm::cos(_phi1) * glm::cos(_theta1), glm::sin(_phi1) * glm::cos(_theta1), glm::sin(_theta1)) * _lr1;
		_anotherLight.ambient = glm::vec3(0.2, 0.2, 0.2);
		_anotherLight.diffuse = glm::vec3(0.8, 0.8, 0.0);
		_anotherLight.specular = glm::vec3(1.0, 1.0, 0.0);

		//=========================================================
		//�������� � �������� �������
		_texture = loadTexture("492SlyusarevData/bogatyri.jpg");

		//=========================================================
		//������������� ��������, �������, ������� ������ ��������� ������ �� ��������
		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("492 Slyusarev Task 2", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
			if (ImGui::CollapsingHeader("Point Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_pointLight.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_pointLight.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_pointLight.specular));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}


			if (ImGui::CollapsingHeader("Camera Light"))
			{
				ImGui::ColorEdit3("cam ambient", glm::value_ptr(_cameraLight.ambient));
				ImGui::ColorEdit3("cam diffuse", glm::value_ptr(_cameraLight.diffuse));
				ImGui::ColorEdit3("cam specular", glm::value_ptr(_cameraLight.specular));
				ImGui::SliderFloat("cam spot size", &(_cameraLight.angle), 0.0f, 0.5f * glm::pi<float>());
			}


			if (ImGui::CollapsingHeader("Another Point Light"))
			{

				ImGui::SliderFloat("another radius", &_lr1, 0.1f, 10.0f);
				ImGui::SliderFloat("another phi ", &_phi1, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("another theta", &_theta1, 0.0f, glm::pi<float>());

				ImGui::ColorEdit3("spot ambient", glm::value_ptr(_anotherLight.ambient));
				ImGui::ColorEdit3("spot diffuse", glm::value_ptr(_anotherLight.diffuse));
				ImGui::ColorEdit3("spot specular", glm::value_ptr(_anotherLight.specular));
			}
		}
		ImGui::End();
	}

	void draw() override
	{
		//�������� ������� ������� ������ � ��������� �������
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//������� ������ ����� � ������� �� ����������� ���������� ����������� �����
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//���������� ������		
		_shader->use();

		//��������� �� ���������� �������� �������-����������
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_pointLight.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_pointLight.position, 1.0));

		_shader->setVec3Uniform("light.pos", lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
		_shader->setVec3Uniform("light.La", _pointLight.ambient);
		_shader->setVec3Uniform("light.Ld", _pointLight.diffuse);
		_shader->setVec3Uniform("light.Ls", _pointLight.specular);

		_shader->setVec3Uniform("cameraLight.pos", glm::vec3(0.0, 0.0, 0.0));
		_shader->setVec3Uniform("cameraLight.dir", glm::vec3(0.0, 0.0, 1.0));
		_shader->setVec3Uniform("cameraLight.La", _cameraLight.ambient);
		_shader->setVec3Uniform("cameraLight.Ld", _cameraLight.diffuse);
		_shader->setVec3Uniform("cameraLight.Ls", _cameraLight.specular);
		_shader->setFloatUniform("cameraLight.cosAngle", glm::cos(_cameraLight.angle));

		_anotherLight.position = glm::vec3(glm::cos(_phi1) * glm::cos(_theta1), glm::sin(_phi1) * glm::cos(_theta1), glm::sin(_theta1)) * _lr1;

		_shader->setVec3Uniform("anotherLight.pos", glm::vec3(_camera.viewMatrix * glm::vec4(_anotherLight.position, 1.0)));
		_shader->setVec3Uniform("anotherLight.La", _anotherLight.ambient);
		_shader->setVec3Uniform("anotherLight.Ld", _anotherLight.diffuse);
		_shader->setVec3Uniform("anotherLight.Ls", _anotherLight.specular);
		glActiveTexture(GL_TEXTURE0);  //���������� ���� 0
		glBindSampler(0, _sampler);
		_texture->bind();
		_shader->setIntUniform("diffuseTex", 0);

		//��������� �� ���������� ������� ������ ����� � ��������� ���������
		{
			_shader->setMat4Uniform("modelMatrix", _moebiusStrip->modelMatrix());
			_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _moebiusStrip->modelMatrix()))));

			_moebiusStrip->draw();
		}

		//������ ������� ��� ���� ���������� �����		
		{
			_markerShader->use();

			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _pointLight.position));
			_markerShader->setVec4Uniform("color", glm::vec4(_pointLight.diffuse, 1.0f));
			_marker->draw();

			_anotherMarkerShader->use();

			_anotherMarkerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _anotherLight.position));
			_anotherMarkerShader->setVec4Uniform("color", glm::vec4(_anotherLight.diffuse, 1.0f));
			_anotherMarker->draw();
		}

		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glUseProgram(0);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}