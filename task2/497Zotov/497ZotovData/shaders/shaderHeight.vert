#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    vec4 absolutePosition = modelMatrix * vec4(vertexPosition, 1.0);
    color.rgb = vec3(absolutePosition.z, 0.0, 1.0 - absolutePosition.z) * 0.5
                + (dot(normalize(vec3(0.1, 0.5, 0.5)), vertexNormal) + 1.0) * vec3(1.0, 1.0, 1.0) * 0.5 * 0.25;
    color.a = 1.0;

    gl_Position = projectionMatrix * viewMatrix * absolutePosition;
}
