#version 330

struct LightInfo
{
    vec3 Dir;
    vec3 La;
    vec3 Ld;
    vec3 Ls;
};

uniform LightInfo light;

uniform sampler2D map_texture;
uniform sampler2D snow_texture;
uniform sampler2D grass_texture;
uniform sampler2D sand_texture;
uniform sampler2D stone_texture;

uniform vec4 specular_color_props;

uniform vec4 light_dir_cam_space;

in vec3 normal_cam_space;
in vec4 position_cam_space;
in vec2 texture_coordinates;
in vec2 map_coordinates;

out vec4 fragment_color;

const float shininess = 128.0;

void main()
{
    vec3 snowColor = texture(snow_texture, texture_coordinates).rgb;
    vec3 grassColor = texture(grass_texture, texture_coordinates).rgb;
    vec3 sandColor = texture(sand_texture, texture_coordinates).rgb;
    vec3 stoneColor = texture(stone_texture, texture_coordinates).rgb;
    vec3 mapColor = texture(map_texture, map_coordinates).rgb;
    float none_color = (0 > 1 - mapColor.r - mapColor.g - mapColor.b) ? 0 : 1 - mapColor.r - mapColor.g - mapColor.b;
    vec3 diffuseColor = mapColor.r * snowColor + mapColor.g * grassColor + mapColor.b * sandColor + none_color * stoneColor;
    vec3 normal = normalize(normal_cam_space);
    vec3 view_direction = normalize(-position_cam_space.xyz);
    float NdotL = max(dot(normal, light_dir_cam_space.xyz), 0.0);
    vec3 color = diffuseColor * (light.La + light.Ld * NdotL);
    if (NdotL > 0.0) 
    {
        vec3 half_vector = normalize(light_dir_cam_space.xyz + view_direction);
        float blinn_term = max(dot(normal, half_vector), 0.0);
        blinn_term = pow(blinn_term, shininess);
        vec3 specular_color = vec3(1, 1, 1) * dot(vec4(mapColor, none_color), specular_color_props);
        color += light.Ls * specular_color * blinn_term;
    }
    fragment_color = vec4(color, 1.0);
}
