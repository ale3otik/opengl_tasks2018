#version 330

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

uniform mat3 normal_to_camera_matrix;

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec2 vertex_texture_coordinate;
layout(location = 3) in vec2 vertex_map_coordinate;

out vec3 normal_cam_space;
out vec4 position_cam_space;
out vec2 texture_coordinates;
out vec2 map_coordinates;

void main()
{
    texture_coordinates = vertex_texture_coordinate;
    map_coordinates = vertex_map_coordinate;
    position_cam_space = view_matrix * model_matrix * vec4(vertex_position, 1.0);
    normal_cam_space = normal_to_camera_matrix * vertex_normal;
    gl_Position = projection_matrix * position_cam_space;
}
