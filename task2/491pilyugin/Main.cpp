#define PI 3.14159265
#include <iostream>

#include <ShaderProgram.hpp>
#include <Application.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include "LSystem.h"
#include <Common.h>
#include "cylinder.h"
#include "leaf.h"


class TreeApplication: public Application {
  public:
   LSystem lTree;
   std::vector<float> cylinderCoord;
   const int numberPolygon = 70;
   const int numLeafs = 50;
   float leafSkipRadius = 1;
   float leafSize = 0.1;
   float leafWith = 0.05;
   float leafHeigth = leafWith/8;

    //Координаты источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

   ShaderProgramPtr _shader;
   ShaderProgramPtr _leafShader;
   std::vector<MeshPtr> cylinderArray;
   std::vector<glm::mat4> modelMatrixLeafs;
   MeshPtr leaf;

   TexturePtr trunkTexture;
   TexturePtr leafTexture;
   GLuint textureTrunkSampler;
   GLuint textureLeafSampler;
   LightInfo _light;

   MeshPtr makeRotateCylinder(glm::vec3 start, glm::vec3 end, float radius, float radiusScale, bool rotateFlag) {
    float height = glm::distance(start, end);
    glm::mat4 modelMatrix;
    glm::vec3 pr(end.x, end.y, start.z);
    // сдвигаем на start точку
    glm::mat4 transMat = glm::translate(glm::mat4(1.0f), start);

    // считаем ось и угол, на который надо повернуть конус
    if (pr != start) {
        float sin = glm::distance(pr, start) / height;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), pr - start);
        // вращаем на нужный угол вокруг оси
        modelMatrix = glm::rotate(transMat, angle, axis);
    } else {
        modelMatrix = transMat;
    }

     MeshPtr cylinder = makeCylinder(radius, radiusScale, height, numberPolygon, rotateFlag);
     cylinder->setModelMatrix(modelMatrix);
     return cylinder;
   }

   void createLeaf(glm::vec3& start, glm::vec3& end, float radius, float scale){
      float height = glm::distance(start, end);
      glm::vec3 pr(end.x, end.y, start.z);

      float position_scale = (float)std::rand() / RAND_MAX;
      glm::vec3 startPoint = start + position_scale * (end - start);
      glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), startPoint);

      if (pr != start) {
        float sin = glm::distance(pr, start) / height;
        float angle = std::asin(sin);
        glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), pr - start);
        modelMatrix = glm::rotate(modelMatrix, angle, axis);
      }

      float angle = (float)std::rand()/RAND_MAX*2*PI;
      glm::vec3 rotAxis = glm::vec3(0.0, 0.0, 1.0);
      modelMatrix = glm::rotate(modelMatrix, angle, rotAxis);
      float indent = (radius + position_scale * (radius*scale - radius)) / 4;
      modelMatrix = glm::translate(modelMatrix, glm::vec3(indent, 0.0, 0.0));
      modelMatrixLeafs.push_back(modelMatrix);

    }

  TreeApplication(): Application() {}

  void makeScene() override {
    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();

    for (int i = 0; i < cylinderCoord.size(); i+=8) {
        glm::vec3 start = glm::vec3(cylinderCoord[i], cylinderCoord[i+1], cylinderCoord[i+2]);
        glm::vec3 end = glm::vec3(cylinderCoord[i+4], cylinderCoord[i+5], cylinderCoord[i+6]);
        //при больших углах нужно перевернуть конус
        bool rotateFlag = swapIfNeed(start, end);
        cylinderArray.push_back(makeRotateCylinder(start, end, cylinderCoord[i+3], lTree.radiusScale, rotateFlag));
        if (!rotateFlag && cylinderCoord[i+3] <= leafSkipRadius)
          for (int i = 0; i < numLeafs; i++) {
            createLeaf(start, end, cylinderCoord[i+3], lTree.radiusScale);
          }
    }

    leaf = makeLeaf(leafSize, leafWith, leafHeigth);

    //Создаем шейдерную программу
    _shader = std::make_shared<ShaderProgram>("491pilyuginData/texture.vert",
    "491pilyuginData/texture.frag");
    _leafShader = std::make_shared<ShaderProgram>("491pilyuginData/texture.vert",
    "491pilyuginData/textureLeaf.frag");

    // Загрузка и создание текстур
    trunkTexture = loadTexture("491pilyuginData/trunk.jpg");
    leafTexture = loadTexture("491pilyuginData/leafTex1.png");

    // Инициализация сэмплеров
    glGenSamplers(1, &textureTrunkSampler);
    glSamplerParameteri(textureTrunkSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(textureTrunkSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(textureTrunkSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(textureTrunkSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(textureTrunkSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 10);

    glGenSamplers(1, &textureLeafSampler);
    glSamplerParameteri(textureLeafSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(textureLeafSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(textureLeafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(textureLeafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(textureLeafSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 10);

    //Инициализация значений переменных освщения

    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    _light.ambient = glm::vec3(0.5, 0.5, 0.5);
    _light.diffuse = glm::vec3(0.6, 0.6, 0.6);
    _light.specular = glm::vec3(0.1, 0.1, 0.1);

  }

  void updateGUI() override
  {
      Application::updateGUI();

      ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
      if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
      {
          ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

          if (ImGui::CollapsingHeader("Light"))
          {
              ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
              ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
              ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

              ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
              ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
              ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
          }

      }
      ImGui::End();
  }

  void draw() override
  {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер дерева
    _shader->use();

    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    GLuint textureUnit = 0;


    _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
    _shader->setVec3Uniform("light.La", _light.ambient);
    _shader->setVec3Uniform("light.Ld", _light.diffuse);
    _shader->setVec3Uniform("light.Ls", _light.specular);
    _shader->setFloatUniform("alpha", 1.0f);


    trunkTexture->bind();
    glBindSampler(textureUnit, textureTrunkSampler);


    _shader->setIntUniform("diffuseTex", textureUnit);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);

    //Рисуем meshes
    for (auto cylin: cylinderArray) {
      _shader->setMat4Uniform("modelMatrix", cylin->modelMatrix());
      cylin->draw();
    }


    //Устанавливаем шейдер листа
    _leafShader->use();

    _leafShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
    _leafShader->setVec3Uniform("light.La", _light.ambient);
    _leafShader->setVec3Uniform("light.Ld", _light.diffuse);
    _leafShader->setVec3Uniform("light.Ls", _light.specular);

    leafTexture->bind();
    glBindSampler(textureUnit, textureLeafSampler);

    _leafShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _leafShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    _leafShader->setFloatUniform("alpha", 0.9f);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (auto modelMatrixLeaf: modelMatrixLeafs) {
      _leafShader->setMat4Uniform("modelMatrix", modelMatrixLeaf);
      _leafShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(
          _camera.viewMatrix * modelMatrixLeaf))));

      leaf->draw();
    }

    glBindSampler(0, 0);
    glUseProgram(0);

  }

  bool swapIfNeed(glm::vec3& a, glm::vec3& b){
    if (b[2] < a[2]) {
      glm::vec3 tmp = a;
      a = b;
      b = tmp;
      return 1;
    } else {
      return 0;
    }
  }

  void initLSystem(
  std::map <char, std::string>	rules,
  std::string	initialString,
  float	startAngle,
  float startDistance,
  float startRadius,
  float	distScale,
  float	angleScale,
  float radiusScale,
  std::string	currentString,
  int numIterations) {
    leafSkipRadius = startRadius * radiusScale * radiusScale;
    lTree = LSystem (rules, initialString,
                    startAngle, startDistance, startRadius, distScale,
                    angleScale, radiusScale, currentString,
                    numIterations);
    lTree.buildSystem();
    cylinderCoord = lTree.calculateLineCoords();
  }

};

int main(int argc, char** argv)
{
  TreeApplication app;
  // std::string initialString = "AB";
  std::string initialString = "F";
  std::map<char, std::string> rules;
  rules['F'] = "F[-F][^F]<[F]>[+F][&F][+F][F]";
  // rules['A'] = "[F[+FCA][-FCA]]";
  // rules['B'] = "[F[>FCB][<FCB]]";
  float startAngle = 60 * 3.1416 / 180;
  float startDistance = 0.8;
  float startRadius = 0.2;
  float distanceScale = 0.75;
  float angleScale = 0.8;
  float radiusScale = 0.4;
  int numIterations = 3;

 app.initLSystem(
   rules,
   initialString,
   startAngle,
   startDistance,
   startRadius,
   distanceScale,
   angleScale,
   radiusScale,
   initialString,
   numIterations);

  app.start();

  return 0;
}
